/*
  |--------------------------------------------------------------------------
  | Google Maps Javascript API
  |--------------------------------------------------------------------------
  | Add the api script of google maps with your ´YOUR_API_KEY´ before the script.js from project
  | Example Pug: ´script(src= "https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY")´
  |
  */
export const googlemaps = (mapName, markerCoordinates) => {
  const element = document.getElementById(mapName)
  const options = {
    zoom: 16,
    //Modificar el centrado del mapa si es necesario
    center: new google.maps.LatLng(markerCoordinates[0].latitude, markerCoordinates[0].longitude)
  }
  const map = new google.maps.Map(element, options);

  markerCoordinates.forEach((coord) => {
    const position = new google.maps.LatLng(coord.latitude, coord.longitude);
    const marker = new google.maps.Marker({
      position,
      map
    });

    const infowindow = new google.maps.InfoWindow()
    marker.addListener('click', function () {
      infowindow.close();
    });
    google.maps.event.addListener(marker, 'click',
      (function (marker, coord, infowindow) {
        return function () {
          infowindow.setContent(coord.title);
          infowindow.open(map, marker);
        };
      })(marker, coord, infowindow));
  });
}