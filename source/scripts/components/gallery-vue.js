import { mainmenu } from "../data/menus"
import { path_media, path_page } from "../data/routes"
export const galleryVue = {
  template: `<div class="gallery l-block utasataki-item">
              <template v-if="mainmenu[idmenu].submenu != null">
                <div class="gallery__item" v-for="item in mainmenu[idmenu].submenu[idsubmenu].gallery">
                  <img :src="path_media + 'images/' + folder + '/' +item.src" alt="">
                </div>
              </template>
              <template v-else>
                <div class="gallery__item" v-for="item in mainmenu[idmenu].gallery">
                  <img :src="path_media + 'images/' + folder + '/' +item.src" alt="">
                </div>
              </template>
             </div>`,
  data() {
    return {
      path_page,
      path_media,
      mainmenu
    }
  },
  props: {
    idmenu: {
      default: 0,
      required: true
    },
    idsubmenu: {
      default: 0
    },
    folder: {
      default: 'living',
      required: true
    }
  }
}