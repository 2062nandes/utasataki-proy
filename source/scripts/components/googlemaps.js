import { path_page, path_media } from "../data/routes";

export const googleMap = {
  template: `<div class="google-map" :id="mapName"></div>`,
  data() {
    return {
      mapName: 'map',
      zoom: 14,
      center: {
        latitude: -16.5272125,
        longitude: -68.0899999
      },
      markerCoordinates: [
        {
          latitude: -16.5152125,
          longitude: -68.1268001,
          title: `<div id="content">
                      <h2 id="firstHeading" class="firstHeading">Utasataki | Sopocachi – LP</h2>
                      <div id="bodyContent" class="center">
                        <img class="map--img" src="${path_media}images/dir-sopocachi.jpg" />
                        <p><b>Dirección:</b> c. Claudio Peñaranda # 2767
                           <br>
                           <b>Tel.:</b> 2421416
                        </p>
                      </div><hr>
                      <footer></footer>
                    </div>`
        },
        {
          latitude: -16.5409881,
          longitude: -68.0670834,
          title: `<div id="content">
                      <h2 id="firstHeading" class="firstHeading">Utasataki | Cota Cota – LP</h2>
                      <div id="bodyContent" class="center">
                        <img class="map--img" src="${path_media}images/dir-cotacota.jpg" />
                        <!-- <p><b>Descripción:</b> Instalación e insumos geosintéticos, tuberías de HDPE y acero.</p> -->
                        <p><b>Dirección:</b> Calle 28 # 100 Edif. Buganvilla<br>
                          <b>Tel.:</b>2792287</p>
                      </div><hr>
                      <footer>
                      </footer>
                    </div>`
        },
        {
          latitude: -16.5400805,
          longitude: -68.052704, 
          title: `<div id="content">
                      <h2 id="firstHeading" class="firstHeading">Utasataki | Chasquipampa – LP</h2>
                      <div id="bodyContent" class="center">
                        <img class="map--img" src="${path_media}images/dir-chasquipampa.jpg" />
                        <p><b>Dirección:</b>Av. Hernando Siles esq. calle 41
                          <br>
                          <b>Tel.:</b> 2777637
                        </p>
                        </div><hr>
                        <footer>
                        </footer>
                        </div>`
                      }
                    ]
                  }
                },
                mounted() {
                  
    const element = document.getElementById(this.mapName)
    const options = {
      zoom: 14,
      center: new google.maps.LatLng(this.center.latitude, this.center.longitude)
    }
    const map = new google.maps.Map(element, options);
    /* MEDIA QUERIE ZOOM GOOGLE MAPS */
    const mediumBp = matchMedia('(min-width: 950px)');
    const changeSize = mql => {
      mql.matches
        ? map.setZoom(14)
        : map.setZoom(11)
    }
    mediumBp.addListener(changeSize)
    changeSize(mediumBp)
    
    this.markerCoordinates.forEach((coord) => {
      const position = new google.maps.LatLng(coord.latitude, coord.longitude);
      const marker = new google.maps.Marker({
        position,
        map
      });
      
      const infowindow = new google.maps.InfoWindow()
      marker.addListener('click', function () {
        infowindow.close();
      });
      google.maps.event.addListener(marker, 'click',
      (function (marker, coord, infowindow) {
        return function () {
          infowindow.setContent(coord.title);
          infowindow.open(map, marker);
          map.setZoom(16)
          map.setCenter(marker.getPosition())
        };
      })(marker, coord, infowindow));
    });

  }
}