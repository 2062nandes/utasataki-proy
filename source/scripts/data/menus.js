export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
    icon: 'icon-home',
    wow_delay: '0.3s'
  },
  {
    title: 'Nosotros',
    href: 'nosotros.html',
    icon: 'icon-us',
    wow_delay: '0.5s'
  },
  // {
  //   title: 'Direcciones',
  //   href: 'direcciones.html',
  //   icon: 'icon-marker',
  //   wow_delay: '0.5s'
  // },
  {
    title: 'Contactos',
    href: 'contactos.html',
    icon: 'icon-contact',
    wow_delay: '0.7s'
  }
]

export const mainmenu = [
  {
    title: 'Livings',
    state: true,
    submenu: [
      {
        title: 'Juegos de living',
        href: 'juegos-living.html',
        src: 'grande.jpg',
        gallery: [
          {
            src: '1.jpg'
          },
          {
            src: '2.jpg'
          },
          {
            src: '3.jpg'
          },
          {
            src: '4.jpg'
          },
          {
            src: '5.jpg'
          },
          {
            src: '6.jpg'
          },
          {
            src: '1.jpg'
          },
          {
            src: '2.jpg'
          }
        ]
      },
      { 
        title: 'Sillones',
        href: 'sillones.html',
        src: 'grande.jpg',
        gallery: [
          {
            src: '1.jpg'
          },
          {
            src: '2.jpg'
          },
          {
            src: '3.jpg'
          },
          {
            src: '4.jpg'
          },
          {
            src: '5.jpg'
          },
          {
            src: '6.jpg'
          },
          {
            src: '7.jpg'
          },
          {
            src: '8.jpg'
          }
        ]
      },
      { 
        title: 'Sillones de Cuero',
        href: 'cuero.html',
        src: 'grande.jpg',
        gallery: [
          {
            src: '1.jpg'
          },
          {
            src: '2.jpg'
          },
          {
            src: '3.jpg'
          },
          {
            src: '4.jpg'
          },
          {
            src: '5.jpg'
          },
          {
            src: '6.jpg'
          },
          {
            src: '7.jpg'
          },
          {
            src: '8.jpg'
          }
        ]
      }      
    ]
  },
  {
    title: 'Juegos de Comedor',
    href: "comedores.html",
    state: false,
    gallery: [
      {
        src: '1.jpg'
      },
      {
        src: '2.jpg'
      },
      {
        src: '3.jpg'
      },
      {
        src: '4.jpg'
      },
      {
        src: '5.jpg'
      },
      {
        src: '6.jpg'
      },
      {
        src: '1.jpg'
      },
      {
        src: '2.jpg'
      }
    ]
  },
  {
    title: 'Dormitorios',
    state: true,
    submenu: [
      {
        title: 'Juegos de dormitorio',
        href: 'juegos-dormitorios.html',
        gallery: [
          {
            src: '1.jpg'
          },
          {
            src: '2.jpg'
          },
          {
            src: '3.jpg'
          },
          {
            src: '4.jpg'
          },
          {
            src: '5.jpg'
          },
          {
            src: '6.jpg'
          },
          {
            src: '1.jpg'
          },
          {
            src: '2.jpg'
          }
        ]
      },
      {
        title: 'Camas',
        href: 'camas.html',
        gallery: [
          {
            src: '1.jpg'
          },
          {
            src: '2.jpg'
          },
          {
            src: '3.jpg'
          },
          {
            src: '4.jpg'
          },
          {
            src: '5.jpg'
          },
          {
            src: '6.jpg'
          },
          {
            src: '1.jpg'
          },
          {
            src: '2.jpg'
          }
        ]
      },
      {
        title: 'Camarotes',
        href: 'camarotes.html',
        gallery: [
          {
            src: '1.jpg'
          },
          {
            src: '3.jpg'
          },
          {
            src: '4.jpg'
          },
          {
            src: '5.jpg'
          },
          {
            src: '6.jpg'
          },
          {
            src: '7.jpg'
          },
          {
            src: '1.jpg'
          },
          {
            src: '3.jpg'
          }
        ]
      }
    ]
  },
  {
    title: 'Muebles de fierro forjado',
    href: "muebles-fierro-forjado.html",
    state: false,
    gallery: [
      {
        src: '1.jpg'
      },
      {
        src: '2.jpg'
      },
      {
        src: '3.jpg'
      },
      {
        src: '4.jpg'
      },
      {
        src: '5.jpg'
      },
      {
        src: '6.jpg'
      },
      {
        src: '7.jpg'
      },
      {
        src: '8.jpg'
      }
    ]
  },
  {
    title: 'Muebles con cerámica',
    href: "muebles-ceramica.html",
    state: false,
    gallery: [
      {
        src: '1.jpg'
      },
      {
        src: '2.jpg'
      },
      {
        src: '3.jpg'
      },
      {
        src: '4.jpg'
      },
      {
        src: '5.jpg'
      },
      {
        src: '6.jpg'
      },
      {
        src: '7.jpg'
      },
      {
        src: '8.jpg'
      }
    ]
  },
  {
    title: 'Detalles',
    href: "detalles.html",
    state: false,
    gallery: [
      {
        src: '1.jpg'
      },
      {
        src: '2.jpg'
      },
      {
        src: '3.jpg'
      },
      {
        src: '4.jpg'
      },
      {
        src: '5.jpg'
      },
      {
        src: '6.jpg'
      },
      {
        src: '7.jpg'
      },
      {
        src: '8.jpg'
      }
    ]
  }  
]