export const dataslide = [
  {
    title: 'Juegos de living',
    description: 'Utasataki - Muebles en pino',
    src: 'images/slide01.jpg'
  },
  {
    title: 'Juegos de Comedor',
    description: 'Utasataki - Muebles en pino',
    src: 'images/slide02.jpg'
  },
  {
    title: 'Dormitorios',
    description: 'Utasataki - Muebles en pino',
    src: 'images/slide03.jpg'
  },
  {
    title: 'Muebles de fierro forjado',
    description: 'Utasataki - Muebles en pino',
    src: 'images/slide04.jpg'
  },
  {
    title: 'Muebles con cerámica',
    description: 'Utasataki - Muebles en pino',
    src: 'images/slide05.jpg'
  },
  // {
  //   title: 'UTASATAKI',
  //   description: 'Utasataki - Muebles en pino',
  //   src: 'images/slide06.jpg'
  // },

]