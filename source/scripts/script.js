import { menuToggle } from "./modules/menu";
import { getdate } from "./modules/date";
import { edTabs } from "./modules/tabs";
import { videoSize } from "./modules/video";

import Headroom from 'headroom.js/dist/headroom.min'
import { options_header } from "./modules/headroom";

import ScrollReveal from 'scrollreveal/dist/scrollreveal.min'

// Slider VanillaJS (https://github.com/ganlanyuan/tiny-slider)
import { tns } from "../../node_modules/tiny-slider/src/tiny-slider.module";
import { options_slider } from "./modules/slider";

import { TweenLite } from "gsap";


// Data Vue
import { mainmenu, menuinicio } from "./data/menus";
import { path_media, path_page } from "./data/routes"
import { dataslide } from "./data/dataslide";

// import Vue from 'vue/dist/vue.min'
import Vue from 'vue/dist/vue'

// import Vue from 'vue/dist/vue'
import VueResource from 'vue-resource/dist/vue-resource.min'
Vue.use(VueResource);

// Vue Components
import { googleMap } from './components/googlemaps'
import { galleryVue } from './components/gallery-vue'
Vue.component('google-map', googleMap)
Vue.component('gallery-vue', galleryVue)

const vm = new Vue({
  el: '#utasataki',
  data: {
    path_media,
    path_page,
    mainmenu,
    menuinicio,
    dataslide
  },
  mounted: function () {
    window.edTabs = edTabs;
    const slider = tns(options_slider);
    menuToggle()
    getdate()
    const header = new Headroom(document.querySelector('#header'), options_header)
    header.init()
    this.animationScroll()
    this.animationLogo()
  },
  methods: {
    animationLogo: function () {
      // TweenLite.from("#logo01", 4, { opacity: 0, left: "300px" });
      // TweenLite.to("#logo02", 4, { opacity: 1, width: "50%", right: "100px", ease: Power2.easeInOut });

      TweenLite.fromTo(
        [
          '#logo01', 
          '#logo02', 
          '#logo03',
          '#logo04',
          '#logo05',
          '#logo06',
          '#logo07',
          '#logo08',
          '#logo09',
          '#logo10',
          '#logo11',
          '#logo12',
        ],
        1.5,
        { opacity: 0, height: '0px' }, 
        { opacity: 1, height: '200px' }
      )
      TweenLite.fromTo(
        [
          '#logo01',
          '#logo02',
          '#logo03',
          '#logo04',
          '#logo05',
          '#logo06',
          '#logo07',
          '#logo08',
          '#logo09'
        ],
        1.5,
        { transform: 'translate(10px, -40px) rotate(50deg)' },
        { transform: 'translate(0, 0) rotate(0)' }
      )
      TweenLite.fromTo(
        ['#logo11'],1.5,
        {
          transform: 'translateY(0)'
        },
        { 
          transform: 'translateY(0)'
        }
      )
    },
    animationScroll: function () {
      window.sr = ScrollReveal({ reset: true })
      // sr.reveal('.main-menu__menu_li', { duration: 900 }, 150)  
      sr.reveal('.tns-outer', { duration: 900, delay: 20 })
      sr.reveal('.foo', { duration: 900 })
      sr.reveal('.gallery__item', { duration: 900 }, 150)
      sr.reveal('.main-footer__item', { duration: 900 }, 150)
      sr.reveal('.card--mini-horizontal', { duration: 900, delay: 20 })
      // sr.reveal('.utasataki-item', { duration: 900 })      
    }    
  }
})